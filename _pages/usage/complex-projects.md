---
layout: page
title: Organising complex projects.
permalink: /usage/complex-projects
---

[step-links]: {{site.baseurl}}/usage/buildup#step-links
[part-links]: {{site.baseurl}}/usage/buildup#part-links
[output-link and fromstep-link pairs]: {{site.baseurl}}/usage/buildup#output-and-fromstep-links


The ordering and data flow of a GitBuilding project is defined by [step-links] that define a subset of the instructions and [part-links] that define parts that are used. A special case of the part-link is the [output-link and fromstep-link pairs].

With these links we can define the instructions for a simple hardware project:

![]({{site.baseurl}}/assets/Steps.png)

***Note:** syntax in diagrams is slightly simplified to make it short enough to fit in the image.*

## Documentation reuse

For a more complex project there may be multiple variations of the same device. To rewrite all documentation risks sections becoming out of date, and duplicates work. One option is to have lots of confusing sentences such as

> If you are building the one version you will need to get this part, however if you are building the other version you will need another.

To avoid this GitBuilding allows the same page to be used for different versions of the project. The navigation and total bill of materials for each project will be calculated separately using the same input pages. This is done by defining a page for each variation and then using step-links to define the instructions to be included:

![]({{site.baseurl}}/assets/Variations.png)

As detailed in the diagram, [fromstep-links][output-link and fromstep-link pairs] can link back to different outputs in the documentation for variations of the project.

## Flexibility

As previously stated, link types define in BuildUp are:

* Standard hyperlinks - simply link to a page
* Step links - Defines the navigation and sums bills of materials for both pages
* Part-links - Adds the part to the bill of materials, but doesn't follow the link.
* Output-link and fromstep-link pairs - Use a part created earlier

By carefully choosing the link types even complex self-replicating machines like the RepRap can be represented:

![]({{site.baseurl}}/assets/Recursive.png)

Note that the link back to the first page was done with a part-link not a step-link. If a step-link had been used this would have created and infinite loop causing an error. This represents reality where to build a self-replicating RepRap, you must already have a RepRap (or another 3D printer).

